import { Component, OnInit } from '@angular/core';
import { StockunitService } from 'src/app/service/stockunit.service';
import { Stockunit } from 'src/app/model/stockunit';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addstock',
  templateUrl: './addstock.component.html',
  styleUrls: ['./addstock.component.css']
})
export class AddstockComponent implements OnInit {

  stockunit:Stockunit;

  constructor(private service:StockunitService,private http:HttpClient,private router:Router) {
    this.stockunit = new Stockunit;
   }

   public createStockUnit(){
    let username='admin'
      let password='admin123'
      const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });
 this.http.post<Stockunit>('http://localhost:8080/stock',this.stockunit,{headers}).subscribe(data=>{
        console.log(this.stockunit);
        this.router.navigate(['/home'])
  })
}

  ngOnInit() {
  }

}
