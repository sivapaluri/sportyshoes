import { Component, OnInit } from '@angular/core';
import { StockunitService } from 'src/app/service/stockunit.service';
import { Stockunit } from 'src/app/model/stockunit';

@Component({
  selector: 'app-men',
  templateUrl: './men.component.html',
  styleUrls: ['./men.component.css']
})
export class MenComponent implements OnInit {

  private stocks:Stockunit[];
  private category:string;

  constructor(private service:StockunitService) {
this.category="men";
   }

  ngOnInit() {
return this.service.getAllStockByCategory(this.category).subscribe(data=>{
this.stocks=data;
console.log(data);

})
  }

}
