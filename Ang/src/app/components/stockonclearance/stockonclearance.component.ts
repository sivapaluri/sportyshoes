import { Component, OnInit } from '@angular/core';
import { StockunitService } from 'src/app/service/stockunit.service';
import { Stockunit } from 'src/app/model/stockunit';

@Component({
  selector: 'app-stockonclearance',
  templateUrl: './stockonclearance.component.html',
  styleUrls: ['./stockonclearance.component.css']
})
export class StockonclearanceComponent implements OnInit {
  private stocks:Stockunit[];

  private clearance:number;
  constructor(private service:StockunitService) { 
    this.clearance=1;
  }

  ngOnInit() {
    return this.service.getAllStockByClearance(this.clearance).subscribe(data=>{
      this.stocks=data;
      console.log(data);
      
      })
  }

}
