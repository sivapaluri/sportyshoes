import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockonclearanceComponent } from './stockonclearance.component';

describe('StockonclearanceComponent', () => {
  let component: StockonclearanceComponent;
  let fixture: ComponentFixture<StockonclearanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockonclearanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockonclearanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
