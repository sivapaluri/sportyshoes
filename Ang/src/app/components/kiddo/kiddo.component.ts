import { Component, OnInit } from '@angular/core';
import { StockunitService } from 'src/app/service/stockunit.service';
import { Stockunit } from 'src/app/model/stockunit';

@Component({
  selector: 'app-kiddo',
  templateUrl: './kiddo.component.html',
  styleUrls: ['./kiddo.component.css']
})
export class KiddoComponent implements OnInit {
  private stocks:Stockunit[];
  private category:string;

  constructor(private service:StockunitService) {
this.category="kids";
   }

  ngOnInit() {
return this.service.getAllStockByCategory(this.category).subscribe(data=>{
this.stocks=data;
console.log(data);

})
  }

}
