import { Component, OnInit } from '@angular/core';
import { Stockunit } from 'src/app/model/stockunit';
import { StockunitService } from 'src/app/service/stockunit.service';

@Component({
  selector: 'app-searchbysport',
  templateUrl: './searchbysport.component.html',
  styleUrls: ['./searchbysport.component.css']
})
export class SearchbysportComponent implements OnInit {
  private stocks:Stockunit[];
  private sport:string;
  constructor(private service:StockunitService) { 
    
  }

  getAllStockBySport(){
    return this.service.getAllStockBySport(this.sport).subscribe(data=>{
      this.stocks=data;
      console.log(data);
  })
}

  ngOnInit() {
    
  }

}
