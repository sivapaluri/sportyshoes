import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchbysportComponent } from './searchbysport.component';

describe('SearchbysportComponent', () => {
  let component: SearchbysportComponent;
  let fixture: ComponentFixture<SearchbysportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchbysportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchbysportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
