import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Stockunit } from 'src/app/model/stockunit';
import { Router } from '@angular/router';

@Component({
  selector: 'app-updatestock',
  templateUrl: './updatestock.component.html',
  styleUrls: ['./updatestock.component.css']
})
export class UpdatestockComponent implements OnInit {

  stockunit:Stockunit;

  constructor(private http:HttpClient, private router:Router) {
    this.stockunit = new Stockunit;
   }

   public updateStockUnit(){
    let username='admin'
      let password='admin123'
      const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });
 this.http.patch<Stockunit>('http://localhost:8080/stock',this.stockunit,{headers}).subscribe(data=>{
        console.log(this.stockunit);
        this.router.navigate(['/home'])
  })
}


  ngOnInit() {
  }

}
