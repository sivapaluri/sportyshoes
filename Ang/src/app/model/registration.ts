export class Registration {

    firstName:string;
    lastName:string;
    age:number;
    sex:string;
    email:string;
    phone:number;
    username:string;
    password:string;
}
