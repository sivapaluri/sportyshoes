export class Stockunit {

    stockId:number;
    category:string;
    size:number;
    price:number;
    image:string;
    colour:string;
    sport:string;
    clearance:number;
}
