import { TestBed } from '@angular/core/testing';

import { StockunitService } from './stockunit.service';

describe('StockunitService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StockunitService = TestBed.get(StockunitService);
    expect(service).toBeTruthy();
  });
});
