import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Registration } from '../model/registration';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {
  url: string;
  registration: Registration;
  constructor(private http: HttpClient) {
  }

  public getRegisteredUserByPhone(phone: number): Observable<Registration[]> {
    let username = 'admin'
    let password = 'admin123'
    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });
    return this.http.get<Registration[]>("http://localhost:8080/reg/" + phone, { headers });
  }


  getAllUsers() {
    let username = 'admin'
    let password = 'admin123'
    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });
    return this.http.get("http://localhost:8080/reg", { headers, responseType: 'json' as 'text' })
  }

}
