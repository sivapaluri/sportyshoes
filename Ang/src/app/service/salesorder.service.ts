import { Injectable } from '@angular/core';
import { SalesOrder } from '../model/sales-order';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SalesorderService {

  constructor(private http:HttpClient) { }

  public getAllSalesOrders(): Observable<SalesOrder[]> {
    let username = 'admin'
    let password = 'admin123'
    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(username + ':' + password) });
    return this.http.get<SalesOrder[]>("http://localhost:8080/order", { headers });
  }
}
