package com.ecommerce.service;

import java.util.List;


import com.ecommerce.model.SalesOrder;


public interface OrderService {
	
	public SalesOrder createOrder(SalesOrder salesorder);
	public List<SalesOrder> getAllOrders();

}
